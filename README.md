![logo](./security-default.png)
<h1 align="center">Welcome to SecurityVPS 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: GPLv3" src="https://img.shields.io/badge/License-GPLv3-yellow.svg" />
  </a>
</p>


> Security Default VPS Debian 11 / Guide by Свободный Волк ;)

#### Создаем ключи ssh (Пароль должен быть стойким)

```sh
~$ ssh-keygen -a 32 -t rsa -b 4096 -C 'libre'
```

#### Копируем наши созданные ключи на тачку

```sh
~$ ssh-copy-id root@195.0.0.201
```

#### Коннектимся к нашей тачке

```sh
~$ ssh root@195.0.0.201
```

#### Обновляем нашу тачку

```sh
~$ apt update && apt upgrade -y
~$ apt -y install sudo whois curl mc git autoconf make tcpdump \
   tree screen htop tree apt-transport-https neofetch net-tools macchanger \
   debsums debsecan fail2ban rkhunter ufw unattended-upgrades
```

#### Включить автоматическое обновление

```sh
~$ apt update && apt -y dist-upgrade
~$ dpkg-reconfigure --priority=low unattended-upgrades
```

#### Редактируем наш баннер

```sh
~$ nano /etc/motd
```

#### Редактируем наш issue на Windows Server

```sh
~$ echo 'Windows Server 2016' > /etc/issue.net
```

#### Создаем нового юзера для входа из под него

```sh
~$ adduser libre
~$ usermod -aG sudo libre
~$ reboot
```
#### В отдельном терминале копируем ключи для нового юзера

```sh
~$ ssh-copy-id libre@195.0.0.201
```

#### Коннектимся к нашей тачке

```sh
~$ ssh root@195.0.0.201
```

#### Install linux libre linux-libre-5.18

```sh
~$ sudo -i

~$ apt update && apt -y upgrade

~$ apt -y install gpgv2 nano curl wget

~$ wget -O - https://mirror.cyberbits.eu/linux-libre/freesh/archive-key.asc | sudo apt-key add -

~$ echo 'deb https://mirror.cyberbits.eu/linux-libre/freesh/ freesh main' >> /etc/apt/sources.list

~$ apt update

~$ apt -y install linux-libre-5.18 linux-libre-5.18-headers

~$ apt autoremove ; reboot

```

#### Protect Grub
##### Этот скрипт позволяет пользователям защитить загрузчик grub от компрометации и установить на него уровень безопасности.

```sh
~$ git clone https://git.disroot.org/librewolf/grubProtect 

~$ cd grubProtect && chmod +x grubProtect.sh

~$ sudo ./grubProtect.sh
```
#### Редактируем наш sshd config

```sh
# https://man.openbsd.org/sshd_config.5

~$ sshd -T 

~$ nano /etc/ssh/sshd_config
```

Заменяем на: [sshd_config](./sshd_config)

```sh
# перезагружаем sshd

~$ service sshd reload
```

#### Редактируем наш sysctl.conf

```sh
~$ nano /etc/sysctl.conf
```

Заменяем на: [sysctl.conf](./sysctl.conf)

```sh
~$ sysctl -p
```

#### Удалить все ключи Диффи-Хеллмана длиной менее 3072 бит

```sh
~$ sudo cp --archive /etc/ssh/moduli /etc/ssh/moduli-COPY-$(date +"%Y%m%d%H%M%S")
~$ sudo awk '$5 >= 3071' /etc/ssh/moduli | sudo tee /etc/ssh/moduli.tmp
~$ sudo mv /etc/ssh/moduli.tmp /etc/ssh/moduli
```

#### Создаем конфиг для удобного коннекта

```sh
Host libre
User libre
port 1337
HostName 195.0.0.201
MACs hmac-sha2-512
KexAlgorithms curve25519-sha256@libssh.org
VisualHostKey no
IdentityFile ~/.ssh/id_rsa

~$ nano ~/.ssh/config

~$ chmod 644 ~/.ssh/config
```
#### Блокировка учетной записи root

```sh
~$ passwd -l root
```
#### Коннектимся к нашей тачке

```sh
~$ ssh libre@195.0.0.201
```

#### Закройте не нужные порты если они есть

```sh
~$ sudo ss -tulpn
```

#### Установите Rkhunter

```sh
Отредактируйте /etc/rkhunter.conf файл с помощью nano

#Enable the mirror checks.
UPDATE_MIRRORS=1

#Tells rkhunter to use any mirror.
MIRRORS_MODE=0

#Specify a command which rkhunter will use when downloading files from the Internet
WEB_CMD=""

~$ sed -i -r -e '/^#|^$/ d' /etc/rkhunter.conf
~$ sudo nano /etc/rkhunter.conf
~$ sudo rkhunter -C
~$ sudo rkhunter --update
~$ sudo rkhunter --propupd 
~$ sudo rkhunter --check --sk 
```

#### Installation Lynis

```sh
~$ cd /usr/local
~$ git clone https://github.com/CISOfy/lynis
~$ cd lynis
~$ ./lynis audit system --quick
```
### TODO:

 * Add script autoInstall  
 * Add Firewall configuration
 * Add Traffic Noisy  
 * More Security  
 * ClamAV

### Author
👤 **Librewolf**

* Open Source: https://t.me/ThisOpenSource

### Show your support

Give a ⭐️ if this project helped you!

### License

`Distributed under the GPL V3 License. See LICENSE for more information`
