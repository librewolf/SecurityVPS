#!/bin/bash

func_check_for_root() {
    if [ ! $( id -u ) -eq 0 ]; then
        echo "ERROR: $0 Must be run as root, Script terminating" ;exit 7
    fi
}

fn_limits() {
cat >>/etc/security/limits.conf <<EOL
*          soft     nproc          999999
*          hard     nproc          999999
*          soft     nofile         999999
*          hard     nofile         999999
EOL
}

func_check_for_root
#fn_limits

page_size=$(getconf PAGE_SIZE)
phys_pages=$(getconf _PHYS_PAGES)
shmall=$(expr $phys_pages / 2)
shmmax=$(expr $shmall \* $page_size)
echo kernel.shmmax = $shmmax
echo kernel.shmall = $shmall
